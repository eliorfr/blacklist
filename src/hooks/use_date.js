import { useState, useEffect } from 'react';
import moment from 'moment';

export const useDate = () => {
  const [now, setNow] = useState(moment());

  useEffect(() => {
    const timer = setInterval(() => {
      setNow(moment());
    }, 30 * 1000);
    return () => {
      clearInterval(timer);
    };
  }, []);

  return now;
};
