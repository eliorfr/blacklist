import { useLayoutEffect } from 'react';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

export const useHideTabBar = (navigation, route) => {
  useLayoutEffect(() => {
    const routeName = getFocusedRouteNameFromRoute(route) || route.name;
    const tabBarVisible = ['Home', 'DietPlan', 'Calendar', 'Documents', 'Account'].includes(routeName);
    navigation.setOptions({ tabBarStyle: { display: tabBarVisible ? 'flex' : 'none' } });
  }, [navigation, route]);
};
