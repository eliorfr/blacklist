import React from 'react';
import EStylesheet from 'react-native-extended-stylesheet';
import { View } from 'react-native';
import { launchImageLibrary } from 'react-native-image-picker';

import ImageProfile from './image_profile';
import { requestPermission } from '../hooks/use_permission';
import { Button } from 'react-native-elements';

const styles = EStylesheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom: 30,
  },
  image: {
    marginLeft: 20,
  },
  button: {
    width: 163,
    height: 28,
    backgroundColor: 'rgba(50, 99, 226, 0.12)',
  },
  text: {
    fontSize: 12,
    color: '$primaryColor',
  },
});

const ProfileEditImage = ({ image, setImage }) => {
  const openImageLibrary = async () => {
    const granted = await requestPermission(['writeExternalStorage', 'readExternalStorage']);
    if (!granted) {
      return;
    }

    launchImageLibrary({}, async fileInfo => {
      console.log(fileInfo);
      setImage('');
    });
  };

  return (
    <View style={styles.container}>
      <Button onPress={openImageLibrary} title={'תמונה חדשה'} />
      <ImageProfile style={styles.image} url={image || ''} size={80} />
    </View>
  );
};

export default ProfileEditImage;
