import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Button as PaperButton } from 'react-native-paper';

const getStyles = ({ mode, uppercase, disabled }) => {
  return EStyleSheet.create({
    button: {
      height: 58,
      justifyContent: 'center',
      borderRadius: 8,
      borderWidth: mode === 'outlined' ? 2 : 0,
      borderColor: disabled ? 'rgba(0,0,0,0.3)' : '$primaryColor',
      backgroundColor: disabled ? 'rgba(0,0,0,0.3)' : '$primaryColor',
    },
    content: {
      height: '100%',
    },
    text: {
      position: 'absolute',
      fontSize: 16,
      fontWeight: '700',
      textAlign: 'center',
      textTransform: uppercase ? 'uppercase' : 'capitalize',
    },
  });
};

const Button = ({ style, children, text, textStyle, ...props }) => {
  const styles = getStyles(props);
  return (
    <PaperButton
      style={[styles.button, style]}
      labelStyle={[styles.text, textStyle]}
      contentStyle={styles.content}
      uppercase={false}
      mode="contained"
      {...props}>
      {text && <Text style={[styles.text, textStyle]}>{text}</Text>}
      {children}
    </PaperButton>
  );
};

Button.propTypes = {
  text: PropTypes.string,
  textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

export default Button;
