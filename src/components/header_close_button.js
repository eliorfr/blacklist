import React from 'react';
import EStylesheet from 'react-native-extended-stylesheet';
import { TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/AntDesign';

const styles = EStylesheet.create({
  headerRight: {
    marginRight: '$appMargin',
  },
});

const HeaderCloseButton = ({ color = 'black', resetToMainTabs = false }) => {
  const navigation = useNavigation();

  const onPress = () => {
    navigation.popToTop();
    if (resetToMainTabs) {
      navigation.goBack();
    }
  };

  return (
    <TouchableOpacity style={styles.headerRight} onPress={onPress}>
      <Icon name="close" size={36} color="black" />
    </TouchableOpacity>
  );
};

export default HeaderCloseButton;
