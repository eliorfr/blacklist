import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';

const styles = EStylesheet.create({
  textCentered: {
    fontSize: 18,
    fontWeight: '500',
  },
  textLeft: {
    marginLeft: '$appMargin',
    fontSize: 28,
  },
});

const HeaderTitle = ({ title = '', isTextCentered = true }) => {
  title = title.length > 25 ? `${title.slice(0, 25)}...` : title;
  return <Text style={[styles.textCentered, !isTextCentered && styles.textLeft]}>{title}</Text>;
};

HeaderTitle.propTypes = {
  title: PropTypes.string,
  isTextCentered: PropTypes.bool,
};

export default HeaderTitle;
