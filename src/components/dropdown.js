import React, { useState, useRef, useEffect } from 'react';
import { View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import ArrowDown from 'react-native-vector-icons/AntDesign';

import DropdownOption from './dropdown_option';
import TextField from './text_field';

const styles = EStyleSheet.create({
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  list: {
    width: '100%',
    marginTop: -10,
    backgroundColor: 'white',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.4)',
    borderWidth: 1,
  },
  icon: {
    flex: 1,
    alignItems: 'flex-end',
  },
});

const Dropdown = ({ containerStyle, options, onSelect, initialValue = '', ...props }) => {
  const [show, setShow] = useState(false);
  const [selected, setSelected] = useState(initialValue);
  const textFieldRef = useRef();

  const openList = () => {
    setShow(v => !v);
  };

  const onSelected = option => {
    setSelected(option.value);
    onSelect(option);
    setShow(v => !v);
  };

  useEffect(() => {
    textFieldRef.current.setValue(selected);
  }, [selected]);

  return (
    <View style={containerStyle}>
      <TouchableOpacity style={styles.button} onPress={openList}>
        <TextField
          ref={textFieldRef}
          value={selected}
          editable={false}
          multiline
          {...props}
          renderRightAccessory={() => (
            <View style={styles.icon}>
              <ArrowDown size={16} name="down" color="black" />
            </View>
          )}
        />
      </TouchableOpacity>
      {show && (
        <View style={styles.list}>
          {options.map(option => (
            <DropdownOption key={option.value} option={option} onSelected={onSelected} />
          ))}
        </View>
      )}
    </View>
  );
};

Dropdown.protoTypes = {
  options: PropTypes.array.isRequired,
  onSelect: PropTypes.func.isRequired,
  initialValue: PropTypes.string,
};

export default Dropdown;
