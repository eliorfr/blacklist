import React, { useState } from 'react';
import EStylesheet from 'react-native-extended-stylesheet';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import ContactAddingModal from './contact_adding_modal';

const styles = EStylesheet.create({
  headerRight: {
    marginRight: '$appMargin',
  },
});

const AddUserButton = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const onPress = () => {
    setModalVisible(v => !v);
  };

  return (
    <>
      <ContactAddingModal modalVisible={modalVisible} setModalVisible={setModalVisible} />
      <TouchableOpacity style={styles.headerRight} onPress={onPress}>
        <Icon name="plus" size={28} color="black" />
      </TouchableOpacity>
    </>
  );
};

export default AddUserButton;
