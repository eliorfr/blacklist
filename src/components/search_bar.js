import React from 'react';
import { TextInput, View } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';

import Icon from 'react-native-vector-icons/AntDesign';

const styles = EStylesheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'rgba(93, 93, 103, 0.06)',
    borderRadius: 4,
    marginHorizontal: '$appMargin',
    height: 40,
  },
  textInput: {
    width: '85%',
    height: '100%',
  },
  icon: {
    marginHorizontal: 10,
  },
});

const SearchBar = props => {
  return (
    <View style={styles.container}>
      <Icon style={styles.icon} name="search1" size={18} color="black" />
      <TextInput
        style={styles.textInput}
        placeholderTextColor="#5D5D67"
        clearButtonMode="while-editing"
        defaultValue=""
        {...props}
      />
    </View>
  );
};

export default SearchBar;
