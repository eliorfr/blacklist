import React, { useState } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';
import { useReduxState } from '../hooks/use_redux_state';
import FastImage from 'react-native-fast-image';
import defaultCoverImage from '../assets/default_cover_image.jpg';
import dayjs from 'dayjs';
import { useNavigation } from '@react-navigation/native';

const styles = EStylesheet.create({
  container: {
    marginTop: 20,
    padding: 10,
    margin: 5,
    borderRadius: 14,
    borderWidth: 1,
    borderColor: 'black',
  },
  title: {
    fontWeight: '500',
    fontSize: 16,
    marginBottom: 8,
  },
  publishedAt: {
    fontWeight: 'bold',
    fontSize: 12,
    marginBottom: 8,
  },
  coverImage: {
    height: 40,
    width: 40,
    marginBottom: 8,
  },
  description: {
    color: 'rgba(0,0,0,0.6)',
    fontSize: 14,
  },
});

const Article = ({ article }) => {
  const navigation = useNavigation();

  const onPress = () => {
    navigation.navigate('FullArticle', { article });
  };

  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Text style={styles.title}>{article.title}</Text>
      <Text style={styles.publishedAt}>{dayjs(article.publishedAt).format('MM/DD/YYYY')}</Text>
      <FastImage style={styles.coverImage} source={article.urlToImage ? { uri: article.urlToImage } : defaultCoverImage} />
      <Text style={styles.description}>{article.description?.slice(0, 80)}</Text>
    </TouchableOpacity>
  );
};

export default Article;
