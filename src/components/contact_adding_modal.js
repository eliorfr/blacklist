import React, { useState } from 'react';
import EStylesheet from 'react-native-extended-stylesheet';
import { Modal, View, Dimensions, ScrollView } from 'react-native';
import TextField from './text_field';
import { useReduxState } from '../hooks/use_redux_state';

import ProfileEditImage from './profile_edit_image';
import Button from './button';
import dayjs from 'dayjs';

const { width, height } = Dimensions.get('window');

const styles = EStylesheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  modalView: {
    backgroundColor: 'white',
    height: height * 0.6,
    width: width * 0.8,
    borderRadius: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  scrollView: {
    flex: 1,
    width: '100%',
    padding: 30,
  },
  scrollViewContent: {
    justifyContent: 'space-between',
    flexGrow: 1,
  },
  width100: {
    width: '100%',
  },
  flex1: {
    flex: 1,
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button: {
    flex: 1,
  },
  close: {
    backgroundColor: 'red',
    marginRight: 20,
  },
});

const ContactAddingModal = ({ modalVisible, setModalVisible }) => {
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [image, setImage] = useState('');
  const [users, setUsers] = useReduxState({ key: 'users' });
  console.log('users', users);

  const saveUser = () => {
    setModalVisible(v => !v);
    const splittedName = name.trim().split(' ');
    const firstName = splittedName[0];
    const lastName = splittedName.length >= 2 ? splittedName.slice(splittedName.length - 1, splittedName.length)[0] : '';
    const middleName = splittedName.slice(1, splittedName.length - 1).join(' ') || '';
    const newUser = { firstName, lastName, middleName, image, phone, createdAt: dayjs(), lastUpdated: dayjs(), debt: [] };
    setPhone('');
    setImage('');
    setName('');
    setUsers([...users, newUser]);
  };

  return (
    <Modal animationType="slide" transparent visible={modalVisible}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <ScrollView style={styles.scrollView} contentContainerStyle={styles.scrollViewContent}>
            <View style={styles.flex1}>
              <ProfileEditImage image={image} setImage={setImage} />
              <TextField inputContainerStyle={styles.width100} onChangeText={text => setName(text)} label="שם" value={name} />
              <TextField
                inputContainerStyle={styles.width100}
                onChangeText={text => setPhone(text)}
                label="טלפון"
                value={phone}
                keyboardType="phone-pad"
              />
            </View>
            <View style={styles.buttons}>
              <Button style={[styles.button, styles.close]} onPress={() => setModalVisible(false)} text={'סגור'} />
              <Button style={styles.button} onPress={saveUser} text={'הוסף'} />
            </View>
          </ScrollView>
        </View>
      </View>
    </Modal>
  );
};

export default ContactAddingModal;
