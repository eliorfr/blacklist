import dayjs from 'dayjs';
import React from 'react';
import { Text, View } from 'react-native';
import { Button, Card } from 'react-native-elements';
import EStylesheet from 'react-native-extended-stylesheet';
import { FlatList } from 'react-native-gesture-handler';
import ImageProfile from './image_profile';

const styles = EStylesheet.create({
  container: {
    flex: 1,
    padding: 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  costAndDate: {
    flexDirection: 'row',
  },
  cost: {
    marginRight: 10,
  },
  date: {
    width: 100,
    textAlign: 'right',
  },
  numOfItems: {
    marginRight: 10,
  },
});

const Debt = ({ debt }) => {
  return (
    <View style={styles.container}>
      <View style={styles.costAndDate}>
        <Text style={styles.numOfItems}>{debt.numOfItems}</Text>
        <Text>{debt.name}</Text>
      </View>
      <View style={styles.costAndDate}>
        <Text style={styles.cost}>{debt.amount}</Text>
        <Text style={styles.date}>{dayjs(debt.date, 'DD/MM/YYYY').format('DD/MM/YY')}</Text>
      </View>
    </View>
  );
};

export default Debt;
