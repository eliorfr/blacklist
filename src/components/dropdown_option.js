import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';

import Touchable from 'react-native-platform-touchable';

const styles = EStyleSheet.create({
  option: {
    padding: 15,
  },
});

const DropdownOption = ({ option, onSelected }) => {
  return (
    <Touchable key={option.label} style={styles.option} onPress={() => onSelected(option)}>
      <Text>{option.label}</Text>
    </Touchable>
  );
};

DropdownOption.protoTypes = {
  option: PropTypes.object.isRequired,
  onSelected: PropTypes.func.isRequired,
};

export default DropdownOption;
