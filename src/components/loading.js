import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';

const styles = EStylesheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const Loading = () => (
  <View style={styles.container}>
    <ActivityIndicator size="large" color="#304ffe" />
  </View>
);

export default Loading;
