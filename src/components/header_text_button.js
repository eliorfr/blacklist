import React from 'react';
import PropTypes from 'prop-types';
import EStylesheet from 'react-native-extended-stylesheet';

import Button from './button';

const styles = EStylesheet.create({
  button: {
    marginHorizontal: '$appMargin',
    backgroundColor: 'rgba(50, 99, 226, 0.12)',
    height: 30,
  },
  text: {
    fontSize: 12,
  },
});

const HeaderTextButton = ({ text, onPress, style = {}, textStyle = {} }) => {
  return (
    <Button
      style={[styles.button, style]}
      text={text}
      onPress={onPress}
      textStyle={[styles.text, textStyle]}
      uppercase
      mode="text"
    />
  );
};

HeaderTextButton.propTypes = {
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  style: PropTypes.object,
  textStyle: PropTypes.object,
};

export default HeaderTextButton;
