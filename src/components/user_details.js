import React from 'react';
import { Text, View } from 'react-native';
import { Button, Card } from 'react-native-elements';
import EStylesheet from 'react-native-extended-stylesheet';
import { FlatList } from 'react-native-gesture-handler';
import Debt from './debt';
import ImageProfile from './image_profile';

const styles = EStylesheet.create({
  container: {
    flex: 1,
    padding: 12,
  },
  contentContainer: {
    flexGrow: 1,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  name: {
    fontSize: 22,
  },
  hr: {
    marginBottom: 4,
    marginTop: 4,
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  },
  total: {
    fontSize: 16,
    alignItems: 'center',
    textAlign: 'center',
  },
  debt: {
    marginTop: 8,
    fontSize: 32,
    fontWeight: 'bold',
  },
  button: {
    marginBottom: 20,
  },
});

const UserDetails = ({ route }) => {
  const { user } = route.params || {};

  return (
    <View style={styles.container}>
      <Card>
        <View style={styles.header}>
          <ImageProfile size={100} />
          <View>
            <Text style={styles.name}>{`${user.firstName} ${user.lastName}`}</Text>
            <View style={styles.hr} />
            <Text style={styles.total}>Total</Text>
            <Text style={styles.debt}>{user.totalDebt}</Text>
          </View>
        </View>
      </Card>
      <FlatList
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
        data={user.debt}
        keyExtractor={item => item.name}
        renderItem={({ item }) => <Debt debt={item} />}
      />
      <Button style={styles.button} title="Add debt" />
    </View>
  );
};

export default UserDetails;
