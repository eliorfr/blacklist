import React from 'react';
import { View } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';
import { FlatList } from 'react-native-gesture-handler';
import User from '../components/user';

import { useReduxState } from '../hooks/use_redux_state';

const styles = EStylesheet.create({
  container: {
    flex: 1,
    padding: 12,
  },
  contentContainer: {
    flexGrow: 1,
  },
});

const Home = () => {
  const [users] = useReduxState({ key: 'users' });

  return (
    <View style={styles.container}>
      <FlatList
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
        data={users}
        keyExtractor={item => item.firstName}
        renderItem={({ item }) => <User user={item} />}
      />
    </View>
  );
};

export default Home;
