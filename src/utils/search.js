import { compareTwoStrings } from 'string-similarity';
import _ from 'lodash';

const getSimilarity = (query, option) => {
  option = option.replace(/[\W_]+/g, ' ');
  const similarities = option
    .split(' ')
    .filter(s => s.length >= 3)
    .concat(option)
    .map(r => compareTwoStrings(r, query));
  return _.max(similarities);
};

export const search = (query, options, key) => {
  if (!query) {
    return [];
  }
  query = query.toLowerCase();
  options = options.map(option => {
    const oldOption = option;
    if (key) {
      option = option[key];
    }
    const lowerCaseOption = option.toLowerCase();
    const similarity = getSimilarity(query, option);
    const startsWith = lowerCaseOption.startsWith(query);
    const includes = query.split(' ').every(w => option.includes(w));
    return { startsWith, includes, similarity, option, oldOption };
  });
  options = options.filter(o => o.startsWith || o.includes || o.similarity >= 0.4);
  options = _.orderBy(options, ['startsWith', 'includes', 'similarity'], ['desc', 'desc', 'desc']);
  return options.map(m => m.oldOption);
};
