import React from 'react';
import { Text } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';

import Home from '../screens/home';
import { primaryColor } from '../utils/colors';
import texts from '../utils/texts';

const MainTabs = () => {
  const Tab = createBottomTabNavigator();

  const tabsIcons = {
    Home: { label: texts.home, icon: 'home' },
  };

  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color, size, focused }) => {
          return <AntDesignIcon size={size} name={tabsIcons[route.name].icon} color={color} />;
        },
        tabBarLabel: ({ focused }) => {
          const color = focused ? primaryColor : '#24242D';
          const fontWeight = focused ? '700' : '500';
          const style = { fontSize: 10, color, fontWeight };
          return <Text style={style}>{tabsIcons[route.name].label}</Text>;
        },
        activeTintColor: primaryColor,
        inactiveTintColor: '#24242D',
      })}>
      <Tab.Screen name="Home" component={Home} options={{ headerShown: false }} />
    </Tab.Navigator>
  );
};

export default MainTabs;
